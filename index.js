function initMap() {
  const map = new google.maps.Map(document.getElementById("map"), {
    
    center: { lat: 22.4502429, lng: 88.3103168 },
    zoom: 8,
    tilt: 30
  });
}