##### HTML 5
```html
<div id="map"></div>

<!-- Replace the dummy API key with an exact API key from Google. -->
<script
  src="https://maps.googleapis.com/maps/api/js?key=AIz...DNA&callback=initMap&libraries=&v=weekly"
  async
></script>
```

##### CSS
```css
#map {
  height: 100%;
}

/* Optional: Makes the sample page fill the window. */
html,
body {
  height: 100%;
  margin: 0;
  padding: 0;
}
```

##### JavaScript
```js
function initMap() {
  const map = new google.maps.Map(document.getElementById("map"), {
    
    center: { lat: 22.4502429, lng: 88.3103168 },
    zoom: 8,
    tilt: 30
  });
}
```